package structsmethodsinterfaces

import "math"

// Shape represents a generic shape
type Shape interface {
	Perimeter() float64
	Area() float64
}

// Rectangle represents a rectangle
type Rectangle struct {
	Width  float64
	Height float64
}

// Perimeter calculates the perimeter of a rectangle
func (r Rectangle) Perimeter() float64 {
	return 2 * (r.Width + r.Height)
}

// Area calculates the area of a rectangle
func (r Rectangle) Area() float64 {
	return r.Width * r.Height
}

// Circle represents a circle
type Circle struct {
	Radius float64
}

// Perimeter calculates the perimeter of a circle
func (c Circle) Perimeter() float64 {
	return math.Pi * 2 * c.Radius
}

// Area calculates the area of a circle
func (c Circle) Area() float64 {
	return math.Pi * c.Radius * c.Radius
}

// Triangle represents a triangle
type Triangle struct {
	Base   float64
	Height float64
}

// Area calculates the area of a triangle
func (t Triangle) Area() float64 {
	return t.Base * t.Height * .5
}

// Perimeter calculates the perimeter of a circle
func (t Triangle) Perimeter() float64 {
	a := t.Base * .5
	b := t.Height
	c2 := (a * a) + (b * b)

	c := math.Sqrt(c2)

	return (2 * c) + t.Base
}
