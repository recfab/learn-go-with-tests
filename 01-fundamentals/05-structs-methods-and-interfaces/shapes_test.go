package structsmethodsinterfaces

import "testing"

func TestPerimeter(t *testing.T) {

	perimeterTests := []struct {
		name         string
		shape        Shape
		hasPerimeter float64
	}{
		{name: "rectangles", shape: Rectangle{Width: 12, Height: 6}, hasPerimeter: 36.0},
		{name: "circles", shape: Circle{Radius: 10}, hasPerimeter: 62.83185307179586},
		{name: "triangles", shape: Triangle{Base: 6, Height: 4}, hasPerimeter: 16},
	}

	for _, tt := range perimeterTests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.shape.Perimeter()
			if got != tt.hasPerimeter {
				t.Errorf("%#v should have perimeter %g, but got %g", tt.shape, tt.hasPerimeter, got)
			}
		})
	}
}

func TestArea(t *testing.T) {

	areaTests := []struct {
		name    string
		shape   Shape
		hasArea float64
	}{
		{name: "rectangles", shape: Rectangle{Width: 12, Height: 6}, hasArea: 72.0},
		{name: "circle", shape: Circle{Radius: 10}, hasArea: 314.1592653589793},
		{name: "triangle", shape: Triangle{Base: 12, Height: 6}, hasArea: 36.0},
	}

	for _, tt := range areaTests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.shape.Area()
			if got != tt.hasArea {
				t.Errorf("%#v should have area %g, but got %g", tt.shape, tt.hasArea, got)
			}
		})
	}
}
