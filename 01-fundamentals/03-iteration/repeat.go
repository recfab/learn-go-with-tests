package iteration

// Repeat takes a character and a number and returns the character repeated that many times
func Repeat(character string, count int) string {
	var repeated string
	for i := 0; i < count; i++ {
		repeated += character
	}
	return repeated
}
