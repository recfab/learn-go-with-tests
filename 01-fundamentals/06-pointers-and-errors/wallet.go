package pointersanderrors

import (
	"errors"
	"fmt"
)

// BitCoin represents BitCoin
type Bitcoin int

func (b Bitcoin) String() string {
	return fmt.Sprintf("%d BTC", b)
}

// Wallet represents a BitCoin wallet
type Wallet struct {
	balance Bitcoin
}

// Deposit adds money to the wallet
func (w *Wallet) Deposit(amount Bitcoin) {
	w.balance += amount
}

// ErrInsufficientFunds occurs when there are insufficient funds to make a withdrawal
var ErrInsufficientFunds = errors.New("cannot withdraw, insufficient funds")

// Withdraw removes from from the wallet
func (w *Wallet) Withdraw(amount Bitcoin) error {
	if amount > w.balance {
		return ErrInsufficientFunds
	}
	w.balance -= amount
	return nil
}

// Balance retrieves the balance of a wallet
func (w *Wallet) Balance() Bitcoin {
	return w.balance
}
