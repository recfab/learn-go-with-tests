package dependencyinjection

import (
	"fmt"
	"io"
)

// Greet writes a greeting to an io.Writer
func Greet(writer io.Writer, name string) {
	fmt.Fprintf(writer, "Hello, %s", name)
}
