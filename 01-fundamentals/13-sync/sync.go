package sync

import "sync"

// Counter represents a counter
type Counter struct {
	mu    sync.Mutex
	value int
}

// NewCounter creates a new Counter
func NewCounter() *Counter {
	return &Counter{}
}

// Inc increments a counter by 1
func (c *Counter) Inc() {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.value++
}

// Value gets the current value of a counter
func (c *Counter) Value() int {
	return c.value
}
