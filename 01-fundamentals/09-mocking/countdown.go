package mocking

import (
	"fmt"
	"io"
	"time"
)

const finalWord = "Go!"
const countdownStart = 3

// Use an interface so we don't actually have to sleep for 4 seconds when running tests

// Sleeper represents something that can sleep for a specified amount of time
type Sleeper interface {
	Sleep()
}

// DefaultSleeper wraps the time package
type DefaultSleeper struct{}

// ConfigurableSleeper is an implementation of Sleeper that can be configured with a duration and a sleep function
type ConfigurableSleeper struct {
	duration time.Duration
	sleep    func(time.Duration)
}

// Sleep calls its sleep function with its duration
func (c *ConfigurableSleeper) Sleep() {
	c.sleep(c.duration)
}

// Countdown prints a countdown to the given io.Writer, with a 1 second delay between each line
func Countdown(writer io.Writer, sleeper Sleeper) {
	for i := countdownStart; i > 0; i-- {
		sleeper.Sleep()
		fmt.Fprintln(writer, i)
	}

	sleeper.Sleep()
	fmt.Fprint(writer, finalWord)
}
