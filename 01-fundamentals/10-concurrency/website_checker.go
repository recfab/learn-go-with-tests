package concurrency

// WebsiteChecker takes a URL and returns a bool indicating success
type WebsiteChecker func(string) bool
type result struct {
	string
	bool
}

// CheckWebsites runs a checker function over a slice of urls and returns a map in the form {url string : success bool}
func CheckWebsites(wc WebsiteChecker, urls []string) map[string]bool {
	results := make(map[string]bool)
	resultChannel := make(chan result)

	for _, url := range urls {
		go func(u string) {
			// `rc <- r` means "send r to the rc channel"
			resultChannel <- result{u, wc(u)}
		}(url)
	}

	for i := 0; i < len(urls); i++ {
		// `r := <-rc` means "receive from rc channel"
		result := <-resultChannel
		results[result.string] = result.bool
	}

	return results
}
