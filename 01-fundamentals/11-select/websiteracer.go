package selectpkg

import (
	"fmt"
	"net/http"
	"time"
)

// Racer takes two URLs and "races" them by hitting them with an HTTP GET and returning the URL which returned first. If none of them return within 10 seconds then it will return an error.
func Racer(a string, b string) (string, error) {
	return ConfigurableRacer(a, b, 10*time.Second)
}

// ConfigurableRacer takes 2 urls and a timeout and "races" them by hitting them with an HTTP GET and returning the URL which returned first. If none of them return within the timeout then it will return an error.
func ConfigurableRacer(a, b string, timeout time.Duration) (winner string, err error) {
	select {
	// What select lets you do is wait on multiple channels. The first one to send a value "wins" and the code underneath the case is executed.
	case <-ping(a):
		return a, nil
	case <-ping(b):
		return b, nil
	case <-time.After(timeout):
		return "", fmt.Errorf("timed out waiting for %s and %s", a, b)
	}
}

// we don't care what type is sent to the channel, we just want to signal we are done and closing the channel works perfectly!
func ping(url string) chan struct{} {
	ch := make(chan struct{})
	go func() {
		http.Get(url) //nolint:errcheck
		close(ch)
	}()
	return ch
}
